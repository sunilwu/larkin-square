<?php
/**
/*  This file gives us back the events slideshow 
/*  for smaller devices
 */
$args = array(
  'post_type' => 'larkin_event'
) ;
$my_query = new WP_Query($args) ;
if ($my_query->have_posts()) : ?>
  <div class="events-row mobile-only">
    <nav class="group">
      <span id="slider-link-next"  class="pointer-right"></span>
      <span  id="slider-link-prev"   class="pointer-left"></span>
    </nav>
    <ul class="cycle-slideshow  event-pane"	
	data-cycle-slides="li"
	data-cycle-prev="#slider-link-prev"
	data-cycle-next="#slider-link-next"
	>
      <?php while ($my_query->have_posts()) : $my_query->the_post();
      ?>
        <li>
          <div class="image-container">
	    <?php   $args = array() ;
		    the_post_thumbnail() ;  ?>
	  </div>
          <div class="day">	  
	    <p><?php echo the_field('event_day')  ?></p>
	    <p><?php echo the_field('event_time')  ?></p>
	  </div>
	</li>
      <?php endwhile; ?>
    </ul>
  </div><?php endif;   return ;  ?>
