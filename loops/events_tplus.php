<?php
/**
/*   This file gives us back the events slideshow for devices
/*    'larger then mobile'.
/*    For smaller devices, it gets hidden
/*    Ideally we wouldn't deliver it all to smaller devices.
 */
$args = array('post_type' => 'larkin_event');
$my_query = new WP_Query($args) ;
?>
  <?php
  $args = array(
    'post-type' => 'larkin_event'
  ) ;
  $my_query = new WP_Query($args) ;

  ?>
<?php if ($my_query->have_posts()) : ?>
  <div class="events-row tplus">
    <?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
      <ul class="event-pane">
        <li>
          <div class="image-container">
            <?php   $args = array() ;
                    the_post_thumbnail() ;  ?>
          </div>
          <div class="day">
            <p><?php echo the_field('event_day')  ?></p>
            <p><?php echo the_field('event_time')  ?></p>
          </div>
        </li>
      </ul>
        <p><a href="<?php echo get_permalink()  ?>">read more</a></p>
      <?php endwhile; ?>
<?php endif; ?>
  </div>  <!-- ENDS .tplus -->  <?php return ;  ?>
