<?php
/*
/*  This file gives us the custom post type defintion
/*  for Larkin Square Events
/*
 */

add_action( 'init', 'register_cpt_larkin_event' );

function register_cpt_larkin_event() {

  $labels = array(
    'name' => _x( 'Events', 'larkin_event' ),
    'singular_name' => _x( 'Event', 'larkin_event' ),
    'add_new' => _x( 'Add New', 'larkin_event' ),
    'add_new_item' => _x( 'Add New Event', 'larkin_event' ),
    'edit_item' => _x( 'Edit Event', 'larkin_event' ),
    'new_item' => _x( 'New Event', 'larkin_event' ),
    'view_item' => _x( 'View Event', 'larkin_event' ),
    'search_items' => _x( 'Search Events', 'larkin_event' ),
    'not_found' => _x( 'No events found', 'larkin_event' ),
    'not_found_in_trash' => _x( 'No events found in Trash', 'larkin_event' ),
    'parent_item_colon' => _x( 'Parent Event:', 'larkin_event' ),
    'menu_name' => _x( 'Events', 'larkin_event' ),
  );

  $args = array(
    'labels' => $labels,
    'hierarchical' => true,
    'description' => 'Larkin Square Events',
    'supports' => array( 'title', 'editor', 'excerpt', 'thumbnail', 'custom-fields', 'comments', 'page-attributes' ),
    'taxonomies' => array( 'category', 'post_tag' ),
    'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'menu_position' => 20,
    'menu_icon' => 'dashicons-calendar',
    'show_in_nav_menus' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => false,
    'has_archive' => true,
    'query_var' => true,
    'can_export' => true,
    'rewrite' => true,
    'capability_type' => 'post'
  );

  register_post_type( 'larkin_event', $args );
}

