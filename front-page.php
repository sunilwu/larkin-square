<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays the front page
 *
 * @package larkin square
 */
get_header('frontpage'); ?>

<div id="primary" class="content-area">
  <main id="main" class="site-main" role="main">

    <article id="frontpage-events">
      <div class="inner-row title-container">
        <h2>Attend an Upcoming Event</h2>
      </div>
      <?php larkinevents_small() ; ?>
      <div class="tplus tplus-events">
	<?php larkinevents_tplus() ;  ?>
      </div>
    </article>  <!-- ENDS #frontpage-events -->

    <article id="filling-station-intro" class="group">
      <header>
	<div class="delim"></div>             
      </header>
      
        <section id="filling-station-calls" class="group">
          <h2>Dine With Us</h2>
          <p class="group">
            <a href="" class="first" >Make a Reservation</a>
            <a href="" class="second">View Our Menu</a>
          </p>
        </section>

    </article>  <!-- ENDS #filling-station-intro -->

    <article id="sponsers">
      <div class="inner-row title-container"><h2>Larkin Square Events</h2></div>
      <div class="inner-row">
        <div class="sponser-container">
        <section>
			<div id="presentedby-big">
				Presented by<br/>
				<a href="/first-niagara"><img src="<?php echo get_template_directory_uri()  ?>/img/sponsors/first-niagara-presentedby-big.png"></a>
			</div>
			<div id="sponsoredby-big">
				Sponsored by<br/>
				<a href="/independent-health"><img src="<?php echo get_template_directory_uri()  ?>/img/sponsors/independent-health-sponsoredby-big.png"></a>
			</div>	 
        </section>
	</div>
      </div>
    </article>

  </main><!-- ENDS #main -->
</div><!-- ENDS #primary -->

<?php get_footer(); ?>
