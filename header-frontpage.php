<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package larkin square
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="generator" content="emacs">
    <meta name="generator" content="Sunil Williams, sunil@sunil.co.nz">
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>
    <?php
    // display the menu called Main Menu
    if ( dynamic_sidebar('mobile_nav_menu') ) : else : endif;
    ?>
    <div id="masthead">
      <div class="inner-row">
	<?php get_template_part('inc/ubermenu')  ?>
      </div>
      <header id="global-header" class="site-header">
        <div class="inner-row group">
          <div class="identity" class="group">
            <a href="<?php echo get_site_url()  ?>" class="logo-container">
              <img src="<?php echo get_template_directory_uri()  ?>/img/larkin-logo.png" alt="Larkin Square"/>
            </a>
          </div>
          <div class="mobile-nav-button-container">
            <a href="#" class="mobile-nav-button"></a>
          </div>
          <div class="nav-and-social tplus">
	    <?php get_template_part('inc/social_links')  ?>
            <?php get_template_part('inc/header_nav_template')  ?>
          </div>
        </div>
      </header><!-- ends #global-header  -->
      <section id="mobile-social-links-header">
      	<?php get_template_part('inc/social_links')  ?>
      </section>
      
      <div class="call-container group" id="throw-a-party">
      <section id="party">
	    <div class="flower-girl"><a href="#"></a></div>
            <header><h3>Throw A Party</h3> </header>
            <p class="message">Larkin Square is perfect for weddings, corporate events, parties and happy hours.</p>
            <footer>
              <p >
                <a href="#">Book your event today</a>
              </p>
            </footer>
          </section>
      </div>    
      <div class="masthead-calls inner-row">
	
        <div class="call-container group">
	  
		  <section id="party-mobile">
			<div class="flower-girl"><a href="#"></a></div>
				<header><h3>Throw A Party</h3> </header>
				<p class="message">Larkin Square is perfect for weddings, corporate events, parties and happy hours.</p>
				<footer>
				  <p >
					<a href="#">Book your event today</a>
				  </p>
				</footer>
			  </section>
		  </div>    
          
	  
          <div class="join-email-list call-container group">
            <section id="join-email-list" class="group">
              <div class="float first">
		<input type="text" name="email" id="email" placeholder="Enter your email address"/>
              </div>
              <div class="float second">
		<span>Join our mailing list</span>
              </div>
            </section>
          </div>
      </div> <!-- ENDS  masthead-calls -->
    </div> <!-- ENDS .masthead -->
    <div id="content" class="site-content">
