<?php
/*
Plugin Name: Larkin Events
Description: Events for Larkin Square website
Version: 0.0.1
Author: Sunil Williams
Author URI: http://sunil.co.nz
License: GPL
 */

if(!defined('ABSPATH')) {
  exit();
}

/**
/*set up constants
 */
define("PLUGIN_NAME", "larkin-events")  ;

/**
/* this is the plug-in directory name
 */
if(!defined("PLUGIN_NAME")) {
  define("PLUGIN_NAME", trim(dirname(plugin_basename(__FILE__)), '/'));
}

/**
/* this is the path to the plug-in's directory
*/
if(!defined("PLUGIN_NAME_DIR")) {
  define("PLUGIN_NAME_DIR", WP_PLUGIN_DIR . '/' . PLUGIN_NAME);
}

/**
 *  This is the url to the plug-in's directory
 */
if(!defined("PLUGIN_NAME_URL")) {
  define("PLUGIN_NAME_URL", WP_PLUGIN_URL . '/' . PLUGIN_NAME);
}

/**
* include our Custom Post Type
*/
include_once("cpt_larkin_events.php") ;

/**
 *   Call our styles and js supporting libs
 */
function larkin_events_scripts() {

  // jquery cycle2
  wp_enqueue_script( 'cycle2', PLUGIN_NAME_URL . "/js/jquery.cycle2.min.js", 'jquery', true);

  //swipe plugin for cycle2
  $deps = array('jquery', 'cycle2') ;
  wp_enqueue_script( 'cycleswipe', PLUGIN_NAME_URL . "/js/jquery.cycle2.swipe.min.js", $deps, true);

  // and our stylesheet
  wp_enqueue_style('larkin_events_stylesheet',  PLUGIN_NAME_URL . "/css/larkin-events.css") ;
}
add_action( 'wp_enqueue_scripts', 'larkin_events_scripts' );


/**
/ *   Now that we've gotten things set up, we can output some actual php into wherever we want.
/ *   In our subdir loops we have some wordpress loops.
/ *   The Following functions will call these loops.
/ *   We can insert these functions into wherever  we want. The functions will spit out our loops
/ *
 */

/**
*  This loop produces code for a slideshow on a smaller screen.
* As long as the css is set up properly, it will be hidden on larger screens
*/
function larkinevents_small(){
  $out = include_once("loops/events_small.php") ;
  echo $out ;
}


// test shortcode export
function larkinevents_tplus(){
  $out = include_once("loops/events_tplus.php");
  echo $out ;
}
add_shortcode("larkinevents_tablet", "larkinevents_tablet" ) ;
