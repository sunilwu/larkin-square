<?php
/**
 * larkin square functions and definitions
 *
 * @package larkin square
 */

class larkin_settings {
  function __construct()  {
    $this -> dirName = dirname(__FILE__) ;
    $this -> baseName = basename(realpath(dirName(__FILE__)));
    $this-> themePath =  get_template_directory_uri() . "/" ;
  }

} // ends settings class

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
  $content_width = 640; /* pixels */
}

if ( ! function_exists( 'larkin_square_setup' ) ) :
 /**
  * Sets up theme defaults and registers support for various WordPress features.
  *
  * Note that this function is hooked into the after_setup_theme hook, which
  * runs before the init hook. The init hook is too late for some features, such
  * as indicating support for post thumbnails.
  */
 function larkin_square_setup() {

  /*
   * Make theme available for translation.
   * Translations can be filed in the /languages/ directory.
   * If you're building a theme based on larkin square, use a find and replace
   * to change 'larkin-square' to the name of your theme in all the template files
   */
  load_theme_textdomain( 'larkin-square', get_template_directory() . '/languages' );

  // Add default posts and comments RSS feed links to head.
  add_theme_support( 'automatic-feed-links' );

  /*
   * Enable support for Post Thumbnails on posts and pages.
   *
   */
  add_theme_support( 'post-thumbnails' );

  // This theme uses wp_nav_menu() in one location.
  register_nav_menus( array(
    'primary' => __( 'Primary Menu', 'larkin-square' ),
  ) );

  // Enable support for Post Formats.
  add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link' ) );

  // Setup the WordPress core custom background feature.
  add_theme_support( 'custom-background', apply_filters( 'larkin_square_custom_background_args', array(
    'default-color' => 'ffffff',
    'default-image' => '',
  ) ) );

  // Enable support for HTML5 markup.
  add_theme_support( 'html5', array(
    'comment-list',
    'search-form',
    'comment-form',
    'gallery',
    'caption',
  ) );
}
endif; // larkin_square_setup
add_action( 'after_setup_theme', 'larkin_square_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function larkin_square_widgets_init() {
  register_sidebar( array(
    'name'          => __( 'Sidebar', 'larkin-square' ),
    'id'            => 'sidebar-1',
    'description'   => '',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h1 class="widget-title">',
    'after_title'   => '</h1>',
  ) );
}
add_action( 'widgets_init', 'larkin_square_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function larkin_square_scripts() {
  wp_enqueue_style( 'larkin-square-style', get_stylesheet_uri() );

  wp_enqueue_script( 'larkin-square-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

  wp_enqueue_script( 'larkin-square-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

  if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
    wp_enqueue_script( 'comment-reply' );
  }
}
add_action( 'wp_enqueue_scripts', 'larkin_square_scripts' );

/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue");
function my_jquery_enqueue() {
  wp_deregister_script('jquery');
  wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") .
                                                                    "://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js", false, null);
  wp_enqueue_script('jquery');
}

/**
 * Add our stylesheets
 */
function larkin_styles()
{

  $settings = new larkin_settings() ;
  $themePath = $settings->themePath ;

  wp_enqueue_style(
    'larkin_styles',
    $themePath  . 'css/larkin.css'
  );
}
add_action( 'wp_enqueue_scripts', 'larkin_styles' );

function larkin_styles_oldschool()
{

  $settings = new larkin_settings() ;
  $themePath = $settings->themePath ;

  wp_enqueue_style(
    'larkin_styles_oldschool',
    $themePath  . 'css/larkin_oldschool.css'
  );
}
add_action( 'wp_enqueue_scripts', 'larkin_styles_oldschool' );


/**
 * mobile navigation menu
 */
function larkin_mobile_nav_menu() {
  $opts = array(
    'name'                         => 'mobile header menu',
    'class'                           => '',
    'before_widget'      =>     '<nav id="mobile-nav-menu">',
    'after_widget'          =>     '</nav>',
    'id'                                  =>      'mobile_nav_menu'
  );
  register_sidebar($opts);
}
add_action('widgets_init', 'larkin_mobile_nav_menu') ;


function larkin_scripts() {
  wp_register_script( 'larkin_main', get_stylesheet_directory_uri() . '/js/app.js', 'jquery' );
  wp_enqueue_script('larkin_main',   get_stylesheet_directory_uri() . '/js/app.js', array('jquery'));
  
} 
add_action( 'wp_enqueue_scripts', 'larkin_scripts' );


add_filter('show_admin_bar', '__return_false');
