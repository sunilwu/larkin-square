jQuery(document).ready(function() {

    var mobileNavMenu  = $("#mobile-nav-menu");
    var mobileNavButton = $(".mobile-nav-button");
    var uberMenu = $("#ubermenu");
    var uberMenu2 = $("#ubermenu2");

    (function(){

	window.larkinApp = {

	    data : {
		mobileNavVisible : 'false',
		uberdropVisible : 'false',	
		uber2dropVisible : 'false'				
	    }, // ends data  

	    functions : {

		toggleNav : function() {
		    if ( window.larkinApp.data.mobileNavVisible == 'false'  ) {
			mobileNavMenu.slideDown();
			window.larkinApp.data.mobileNavVisible = 'true' ;
			// ... and change state of nav button
			console.debug(mobileNavButton) ;
			$(".mobile-nav-button").css("background-position", "100% -80px");
			//mobileNavButton.css("background-position", "100% -80");
		    } else {
			mobileNavMenu.slideUp();
			window.larkinApp.data.mobileNavVisible = 'false' ;			
			mobileNavButton.css("background-position", "100% -2px");
		    }
		    
		},  // ends toggleNav

		uberMenuShowHide : function(){
		    console.debug("got to showhide");
		    if (window.larkinApp.data.uberdropVisible == 'false' ) {
				uberMenu.fadeIn();
				window.larkinApp.data.uberdropVisible = 'true';
				if (window.larkinApp.data.uber2dropVisible == 'true' ) {
					uberMenu2.fadeOut();
					window.larkinApp.data.uber2dropVisible = 'false';
		    	}
		    }
		    else {
				uberMenu.fadeOut();
				window.larkinApp.data.uberdropVisible = 'false';
		    }
		    console.debug("uberMenuShowHide is done") ;
		}, 
		uberMenu2ShowHide : function(){
		    console.debug("got to showhide2");
		    if (window.larkinApp.data.uber2dropVisible == 'false' ) {
				uberMenu2.fadeIn();
				window.larkinApp.data.uber2dropVisible = 'true';
				if (window.larkinApp.data.uberdropVisible == 'true' ) {
					uberMenu.fadeOut();
					window.larkinApp.data.uberdropVisible = 'false';
		    	}				
		    }
		    else {
				uber2Menu.fadeOut();
				window.larkinApp.data.uber2dropVisible = 'false';
		    }
		    console.debug("uberMenu2ShowHide is done") ;
		    		    
		} // ends findDropDownTrigger
		
	    },  //ends functions

	    init  : {
				
		tests : function() {
		    console.debug("got to test") ;
		},  // ends tests

		uberMenu: function() {
		    console.debug("got to uberMenu") ;
		    var  trigger =  $(".header-nav .page-item-4") ;
		    
		    trigger.on("mouseover", function(e) {
			e.preventDefault() ;
			window.larkinApp.functions.uberMenuShowHide();
		    });

		    uberMenu.on("mouseleave", function() {
			console.debug("leaving!");
			uberMenu.fadeOut();
			window.larkinApp.data.uberdropVisible = 'false';
		    });
		    
		}, // ends uberMenu

		uberMenu2: function() {
		    console.debug("got to uberMenu2") ;
		    var  trigger2 =  $(".header-nav .page-item-5") ;
		    
		    trigger2.on("mouseover", function(e) {
			e.preventDefault() ;
			window.larkinApp.functions.uberMenu2ShowHide();
		    });

		    uberMenu2.on("mouseleave", function() {
			console.debug("leaving!");
			uberMenu2.fadeOut();
			window.larkinApp.data.uber2dropVisible = 'false';
		    });
		    
		} // ends uberMenu2		
	    }  // ends init
	    
	};

	window.larkinApp.init.uberMenu() ;
	window.larkinApp.init.uberMenu2() ;	
    })();  // ends our iife


    mobileNavButton.on("click", function() {
	larkinApp.functions.toggleNav();
    })
    
});
