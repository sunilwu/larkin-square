/**
 * navigation.js
 *
 * Handles toggling the navigation menu for small screens.
 */
( function() {
    var container, button, menu;

    container = document.getElementById( 'site-navigation' );
    if ( ! container )
        return;

    //button = container.getElementsByTagName( 'button' )[0];
    button = $(".mobile-nav-button-container a")
    if ( 'undefined' === typeof button )
        return;

    //menu = container.getElementsByTagName( 'ul' )[0];
    menu = $("#mobile-name-menu") ;

    if ( -1 === menu.className.indexOf( 'nav-menu' ) )
        menu.className += ' nav-menu';

    button.onclick = function() {
	console.debug("got here");
        if ( -1 !== container.className.indexOf( 'toggled' ) ) {
            container.className = container.className.replace( ' toggled', '' );
	}
        else
            container.className += ' toggled';
    };
} )();
