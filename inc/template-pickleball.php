	<?php
/**
 * The template for the "Pickleball Rules" page
 *
 * Template Name: Pickleball Rules
 *
 * @package larkin square
 */

get_header('pickleball'); ?>

<div id="primary" class="content-area">
  <main id="main" class="site-main" role="main">

    <div class="inner-row">
      <?php while ( have_posts() ) : the_post(); ?>

        <article id="post-<?php the_ID(); ?>" <?php post_class('pickleball-post'); ?>>
          <div class="entry-content">
            <?php the_content(); ?>           
          </div><!-- .entry-content -->
	</article><!-- #post-## -->
      <?php endwhile; // end of the loop. ?>

      <?php get_template_part('inc/sidebar-pickleball')  ?>
      
    </div>   <!-- ENDS .inner-row -->

  </main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>
