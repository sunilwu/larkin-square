<?php
/**
 * The template for the "Pickleball Rules" section
*  This can be used as a sidebar, or inserted into other templates
 *
 *
 * @package larkin square
 */
?>

<article id="pickleball-rules-table">
  <div class="bats">
    <img src="<?php echo get_template_directory_uri()  ?>/img/illustrations/crossed-bats.png"
	 alt="pickeball-bats"/>
  </div>
  <header>
    <h2>Official</h2>
    <h3>Pickleball Rules</h3>
  </header>
  <ol id="list-pickleball-rules">
    <li><p>Serve the ball underhand, diagonally, cross-court, bouncing inside the court beyond the non-volley zone.</p></li>
    <li><p>One foot must be kept behind the back line when serving.</p></li>
    <li><p>The ball must bounce once on each side after serving.</p></li>
    <li><p>Once it has bounced on both sides, the ball may be returned from a bounce or a volley.</p></li>
    <li><p>Players cannot hit a volley in the first 7' from the net (non-volley zone).</p></li>
    <li><p>Points can only be scored when serving.</p></li>
    <li><p>A point is scored when the opponent faults by missing the ball, hitting it in the net or out of bounds, hitting a volley in the non-volley zone, or volleying the ball before it has bounced once on each side of the net.</p></li>
    <li><p>Only one service attempt is allowed (except in the event of a ""let,"" when the ball touches the net and lands on other side).</p></li>
    <li><p>In doubles, each member of the serving team serves until they lose the service. Once both players have lost the serve, the service goes to opposing team.</p></li>
    <li><p>At the beginning of a game, the serving side relinquishes the serve after first fault.</p></li>
    <li><p>A game is played to 11 points; a 2-point</p></li>
  </ol>    
</article>  <!-- ENDS pickleball-rules-table -->
