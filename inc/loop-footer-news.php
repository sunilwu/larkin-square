<?php
/*
 *  This template gives us the links to blog news entries
 *  in the global page footer.
 *
 *  Sunil Williams <sunil@sunil.co.nz>
 *
 */
?><section id="footer-news">
<header><h3>News From The Larkin Square Blog</h3> </header>

<?php
$args = array(
  'posts_per_page' => 3,
  'post_type'    => 'post'
) ;
$my_query = new WP_Query($args) ;
$count =0 ;
if ($my_query->have_posts()) : ?>
  <?php while ($my_query->have_posts()) :
   $my_query->the_post();
	$count++ ;
  ?>
    <article<?php
    if ( $count == 3) {  ?> class="last" <?php }  ?>>
    <p><a href="<?php echo get_the_permalink()?>"> <?php the_title()  ?></a></p>
    </article>
    <?php endwhile; ?>
<?php endif; ?>
</section>
