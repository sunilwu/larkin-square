<?php
/**
 * The template for the "Contact" page
 *
 * Template Name: Contact
 *
 * @package larkin square
 */

get_header(); ?>

<div id="primary" class="content-area">
  <main id="main" class="site-main" role="main">

    <div class="inner-row">
<article >
      <?php while ( have_posts() ) : the_post(); ?>
	<?php  get_template_part( 'content', 'page' ); ?>
    <?php endwhile; // end of the loop. ?>
    </article>  
    
    
    <div class="sidebar-half">
    <h3>Come Visit Us</h3>
	Larkin Square is centrally located in the Larkin District, just one block north of the Larkin at Exchange Building,
	at the intersection of Swan and Seneca Streets.<br/><br/>
    <h3>Address</h3>
	Larkin Square
	745 Seneca Street<br/>
	Buffalo, NY 14210<br/><br/>
	<h3>Phone Number</h3>
	716-362-2665

    </div>   <!-- ENDS .inner-row -->

  </main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>
