<?php
/**
 *  This template gives us a collection of posts of the event type,
 *  and of the category "food truck Tuesdays"
 *
 */?>
<section id="live-music-events">
<div class="inner-row">
  <header>
    <div id="accordion">
    <img src="<?php echo get_template_directory_uri()  ?>/img/illustrations/accordion.jpg" alt="Live Music List"/>
    </div>
    <h2>Live Music At Food Truck Tuesdays</h2>
  </header>
  <?php
  $cat_name = "Food Truck Tuesdays" ;    
  $args = array(
    'post_type'        => 'larkin_event',
    'category_name' =>  "Food Truck Tuesdays"
  ) ;
  $my_query = new WP_Query($args) ;
  ?>
  <?php if ($my_query->have_posts()) : ?>
    <ul id="events-list" class="group">
      <?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
        <li>
          <h3> <a href="<?php the_permalink()  ?>"><?php the_title()  ?></a></h3>
	  <p><?php echo the_field('event_day')  ?></p>
          <div class="excerpt">
	  <?php  the_excerpt(); ?>
	  </div>
        </li>
      <?php endwhile; ?>
    </ul> <!-- ENDS #...  -->
  <?php endif; ?>
</div><!-- ENDS .inner-row -->
</section>
