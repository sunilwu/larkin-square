<?php
/**
 * The template for the "Filling Station" page
*
 * Template Name: Filling Station
*
 * @package larkin square
 */
get_header(); ?>
<div id="primary" class="content-area">
  <main id="main" class="site-main" role="main">

    <div class="inner-row">
    <article >
      <?php while ( have_posts() ) : the_post(); ?>
	<?php  get_template_part( 'content', 'page' ); ?>
    <?php endwhile; // end of the loop. ?>
    </article>    
    <?php
    ?>


    </div>
</div>
  </main><!-- #main -->
</div><!-- #primary -->
<?php get_footer(); ?>
