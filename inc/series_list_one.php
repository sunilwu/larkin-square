<div class="live-at-larkin-sidebar">
  <article id="live-series">
    <header>
      <h2>Live at Larkin</h2>
      <h3>Summer Concert Series</h3>
    </header>
    <ul>
      <li>
        <span class="date">June 18</span>
        <span class="event">Neville Francis and the Riddim Posse + Vitamin D</span>
      </li>
      <li>
        <span class="date">June 25</span>
        <span class="event">The Albrights + Aircraft</span>
      </li>
      <li>
        <span class="date">July 2</span>
        <span class="event">The Ragbirds + The Doyle Brothers</span>
      </li>
      <li>
        <span class="date">July 9</span>
        <span class="event">John & Mary + The Valkyries</span>
        <span class="event">Michael Oliver + His Sacred Band</span>
      </li>
      <li>
        <span class="date">July 16</span>
        <span class="event">Dive House Union + Jony James Band</span>
      </li>
      <li>
        <span class="date">July 23</span>
        <span class="event">Aqueous  + Jeff Miers Band</span>
      </li>
      <li>
        <span class="date">July 30</span>
        <span class="event">Argyle Street Band + Leroy Townes</span>
      </li>
      <li>
        <span class="date">August 6</span>
        <span class="event">The Steam Donkeys  + Shakey Stage</span>
      </li>
      <li>
        <span class="date">August 13</span>
        <span class="event">Brian Higgins & The Exchange Street Band</span>
        <span class="event">The Jack Mahones + The Charlie O'Neill Band Unplugged</span>
      </li>
      <li>
        <span class="date">August 20</span>
        <span class="event">Brian Higgins & The Exchange Street Band</span>
        <span class="event">The Jack Mahones + The Charlie O'Neill Band Unplugged</span>
      </li>
      <li>
        <span class="date">August 27</span>
        <span class="event">Back To School Night with WNY Hight School Musicians & Music Is Art</span>
      </li>
      <li>
        <span class="date">September 3</span>
        <span class="event">Mary C & The Cellars + </span>
        <span class="event">Drea d'Nur </span>
      </li>
      <li>
        <span class="date">September 10</span>
        <span class="event">Peter Case + Tom Stahl  & The Dangerfields</span>
      </li>
      <li>
        <h4>Total Request Larkin</h4>
        <span class="date">September 17</span>
        <span class="event">Lance Diamond & The 24 Carat Diamond Band + Caitlan Koch</span>
      </li>
    </ul>
  </article>

  <article id="live-at-larkin-sponsors">
    <h2>Live at Larkin is sponsored by</h2>
    <div class="sponsor-list group">
	<a href="#" class="link first-niagara"></a>
	<a href="#" class="link independent-health"></a>
      </div>
  </article> <!-- ENDS #live-at-larkin-sidebar -->
  
</div>  <!-- ENDS live-at-larkin-sidebar -->
