<?php
/**
 * The template for the "Live At Larkin" page
*
 * Template Name: Live At Larkin
*
 * @package larkin square
 */
get_header(); ?>
<div id="primary" class="content-area">
  <main id="main" class="site-main" role="main">

    <div class="inner-row">
    <article >
      <?php while ( have_posts() ) : the_post(); ?>
	<?php  get_template_part( 'content', 'page' ); ?>
    <?php endwhile; // end of the loop. ?>
    </article>    
    <?php
    /**
    /* Now get the listing
    */
    get_template_part("inc/series_list_one")
    ?>

    </div>
</div>
  </main><!-- #main -->
</div><!-- #primary -->
<?php get_footer(); ?>
