<?php
/**
 * The template for the "Rules and Regulations" page
*
 * Template Name: Rules and Regulations
*
 * @package larkin square
 */
get_header(); ?>
<div id="primary" class="content-area">
  <main id="main" class="site-main" role="main">

    <div class="inner-row">
    <article >
      <?php while ( have_posts() ) : the_post(); ?>
	<?php  get_template_part( 'content', 'page' ); ?>
    <?php endwhile; // end of the loop. ?>
    </article>    
    <?php
    /**
    /* Now get the listing
    */ ?>
    <div class="sidebar-third">
    Larkin Development Group is pleased to invite the public to enjoy Larkin Square year-round, seven days a week. Thank you for following some simple rules to ensure everyone’s enjoyment of Larkin Square. 
	<br/><br/>
	<div id="larkinsquareguidelines">
		<h3>Larkin Square Guidelines</h3>
		No smoking<br/>
		No coolers<br/>
		No soliciting<br/>
		No Pets<br/>
		No Skateboards,<br/>
		rollerskating or bike riding<br/>
		within the Square.
	</div><br/>
	Alcoholic beverages may not be brought into Larkin Square from the outside. You are welcome to enjoy a beverage purchased from The Filling Station, The Larkin Square Grill, the Beer Cart, or other Larkin vendors within the Square. You may not take open containers of alcohol outside the boundaries of Larkin Square. 
	<br/><br/><br/>
	<img class="alignleft size-full wp-image-137" src="http://newlarkinsquare.dreamhosters.com/wp-content/uploads/2014/05/bar.png" alt="bar" width="151" height="97" />
    </div>


    </div>
</div>
  </main><!-- #main -->
</div><!-- #primary -->
<?php get_footer(); ?>
