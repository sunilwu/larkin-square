<?php
/***
 *
 *  This template gives us the navigation menu in the header,
 *  visible when the 'tablet' media query is triggered
 */
?>
<nav class="header-nav group">
  <?php
  $args = array (
    'title_li'            => '',
    'post_type'   => 'page',
    'depth'             => '1'
  );
  /**
  /* uncomment this line if you want a programmatically generated menu here
  /*wp_page_menu($args) ;
   */
  ?>
  <ul>
    <li><a href="<?php echo get_site_url()  ?>"/>Home</a></li>
    <li class="page-item-4"><a href="<?php echo get_site_url()  ?>/about">About</a></li>
   <li class="page-item-5"><a href="<?php echo get_site_url()  ?>/things-to-do"">Things To Do</a></li>
   <li><a href="<?php echo get_site_url() ?>/blog">Blog</a></li>
   <li><a href="<?php echo get_site_url()  ?>/contact">Contact</a></li>   
  </ul>
</nav>
