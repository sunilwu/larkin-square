<?php
/****
*  This template gives us image links to 
*  our sponsers websites
 */?>
<div class="inner-row sponsers-row ">
<article id="include-sponsors">
    <div class="sponser-container">
      <section>
        <div id="presentedby-big">
	  <h3>Presented by</h3>
          <a href="/first-niagara"><img src="<?php echo get_template_directory_uri()  ?>/img/sponsors/first-niagara-presentedby-big.png"></a>
        </div>
        <div id="sponsoredby-big">
          Sponsored by<br/>
          <a href="/independent-health"><img src="<?php echo get_template_directory_uri()  ?>/img/sponsors/independent-health-sponsoredby-big.png"></a>
        </div>
      </section>
    </div>
</article>
</div>
