<?php
/**
 * The template for the "In the Neighborhood" page
*
 * Template Name: In the Neighborhood
*
 * @package larkin square
 */
get_header(); ?>
<div id="primary" class="content-area">
  <main id="main" class="site-main" role="main">

    <div class="inner-row">
    <article >
      <?php while ( have_posts() ) : the_post(); ?>
	<?php  get_template_part( 'content', 'page' ); ?>
    <?php endwhile; // end of the loop. ?>
    </article>    
    <?php
    /**
    /* Now get the listing
    */
    
    ?>

    </div>
</div>
  </main><!-- #main -->
</div><!-- #primary -->
<?php get_footer(); ?>
