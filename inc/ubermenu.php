<?php
/**
/*  This template renders the large dropdown navigation menu
 */
?>
<nav id="ubermenu" class="group">
  <section class="left">    
    <header>
      <a href="/about"><h3>About Larkin Square</h3></a>
    </header>
    <div class="hoop"></div>
    <div id="presentedby">
		Larkin Square Events<br/>
		Presented by<br/>
		<a href="/first-niagara"><img src="<?php echo get_template_directory_uri()  ?>/img/sponsors/first-niagara-presentedby.png"></a>
    </div>
    <div id="sponsoredby">
		Sponsored by<br/>
		<a href="/independent-health"><img src="<?php echo get_template_directory_uri()  ?>/img/sponsors/independent-health-sponsoredby.png"></a>
    </div>
  </section>
  <section class="right">
    <div class="nav-left">
      <ul>
        <li><a href="><?php echo get_site_url();  ?>/in-the-neighborhood">In the Neighborhood</a></li>
		<li><a href="<?php echo get_site_url();  ?>/pickleball-courts">Pickleball Courts</a></li>
		<li><a href="<?php echo get_site_url();   ?>/larkinville-gardens">Larkinville Gardens</a></li>
		<li><a href="<?php echo get_site_url();   ?>/history-of-larkin">History of Larkin</a></li>
		<li><a href="<?php echo get_site_url();  ?>/larkin-development-group">Larkin Development Group</a></li>
		<li><a href="<?php echo get_site_url();  ?>/rules-and-regulations">Rules and Regulations</a></li>
      </ul>
    </div>
    <div class="nav-right">
      <ul>
  	  <li><a href="<?php echo get_site_url();  ?>/directions-and-parking">Directions and Parking</a></li>
      <li><a href="<?php echo get_site_url();  ?>/press-coverage">Press Coverage</a></li>
      <li><a href="<?php echo get_site_url();  ?>/photo-gallery">Photo Gallery</a></li>
      </ul>
    </div>
  </section>
</nav>

<nav id="ubermenu2" class="group">
  <section class="ubermenu2-left">    
   <div>
      <ul>
		<li><a href="<?php echo get_site_url();  ?>/events-calendar">Events Calendar</a></li>
		<li><a href="<?php echo get_site_url();   ?>/private-events">Book a Private Event</a>
		  <ul class="indented">
			<li class="indented"><a href="<?php echo get_site_url();  ?>/weddings">Weddings</a></li>
			<li class="indented"><a href="<?php echo get_site_url();   ?>/corporate-events">Corporate Events</a>
			<li class="indented"><a href="<?php echo get_site_url();   ?>/happy-hour">Happy Hour</a>
		 </ul>
		</li>
	</ul>
	</div>
  </section>
  <section class="ubermenu2-right">
    <div>
    	<div class="nav-ubermenu2-callouts">
    		<a href="<?php echo get_site_url();  ?>/filling-station">
    			<div class="nav-ubermenu-callout-image" id="nav-ubermenu-fillingstation-image"></div>
    			<div class="nav-ubermenu-callout-text">Eat at the Filling Station</div>
    		</a>
    	</div>
    	<div class="nav-ubermenu2-callouts">
    		<a href="<?php echo get_site_url();  ?>/author-series">
    			<div class="nav-ubermenu-callout-image" id="nav-ubermenu-authorseries-image"></div>
    			<div class="nav-ubermenu-callout-text">Larkin Square Author Series</div>
    		</a>
    	</div>
    	<div class="nav-ubermenu2-callouts">
    		<a href="><?php echo get_site_url()  ?>/foodtruck-tuesdays">
    			<div class="nav-ubermenu-callout-image" id="nav-ubermenu-foodtrucktuesdays-image"></div>
    			<div class="nav-ubermenu-callout-text">Food Truck Tuesdays</div>
    		</a>
    	</div>
    	<div class="nav-ubermenu2-callouts">
    		<a href="<?php echo get_site_url();  ?>/live-at-larkin">
    			<div class="nav-ubermenu-callout-image" id="nav-ubermenu-liveatlarkin-image"></div>
    			<div class="nav-ubermenu-callout-text">Live at Larkin</div>
    		</a>
    	</div>
    	<div class="nav-ubermenu2-callouts">
    		<a href="<?php echo get_site_url();  ?>/pickleball-courts">
    			<div class="nav-ubermenu-callout-image" id="nav-ubermenu-pickleball-image"></div>
    			<div class="nav-ubermenu-callout-text">Pickleball</div>
    		</a>
    	</div>
    	<div class="nav-ubermenu2-callouts">
    		<a href="<?php echo get_site_url();  ?>/larkin-marketplace">
    			<div class="nav-ubermenu-callout-image" id="nav-ubermenu-marketplace-image"></div>
    			<div class="nav-ubermenu-callout-text">Marketplace</div>
    		</a>
    	</div>
    </div>
  </section>
  <section class="bottom">    
   <div class="nav-bottom">
	</div>
  </section>  
</nav>
