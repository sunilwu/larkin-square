<?php
/**
 * The template for the "Independent Health" page
 *
 * Template Name: Independent Health
 *
 * @package larkin square
 */

get_header(); ?>

<div id="primary" class="content-area">
  <main id="main" class="site-main" role="main">

    <div class="inner-row">
<article >
      <?php while ( have_posts() ) : the_post(); ?>
	<?php  get_template_part( 'content', 'page' ); ?>
    <?php endwhile; // end of the loop. ?>

	
    </article>  
    
    
    <div class="sidebar-third">
	    <div class="grey-box">    
	    	<h3>Connect with Independent Health</h3> 
			<br/>
			Like Independent Health on <a href="http://www.facebook.com/independenthealth" target="_blank"><img src="<?php echo get_template_directory_uri()  ?>/img/icon-facebook.png"></a> or follow us on <a href="http://www.twitter.com/IH_news" target="_blank"><img src="<?php echo get_template_directory_uri()  ?>/img/icon-twitter.png"></a><br/>
			<br/>
			Like the Independent Health Foundation on <a href="http://www.facebook.com/independenthealthfoundation" target="_blank"><img src="<?php echo get_template_directory_uri()  ?>/img/icon-facebook.png"></a><br/>
			<br/>
			For information on making informed decisions when dining out, follow Healthy Options Buffalo on <a href="http://www.twitter.com/healthyoptbuff" target="_blank"><img src="<?php echo get_template_directory_uri()  ?>/img/icon-twitter.png"></a> or visit <a href="http://www.HealthyOptionsBuffalo,com" target="_blank">HealthyOptionsBuffalo.com</a>.<br/>

	    	
		</div>
    </div>   <!-- ENDS .inner-row -->

  </main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>
