<?php
/**
 * The template for the page "Food Truck Tuesdays"
 * Template Name: Food Truck Tuesdays
 *
 * @package larkin square
 *
 */  ?>
<?php get_header(); ?>
<div class=" group">
  <div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
      <div class="main-and-sidebar">
        <div class="main-post">
          <div class="inner-row">
            <?php while ( have_posts() ) : the_post(); ?>
              <article id="post-<?php the_ID(); ?>" <?php post_class('post'); ?>>
                <header class="entry-header">
                  <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
                </header><!-- .entry-header -->
                <div class="entry-content">
                  <?php the_content(); ?>
                  <?php
                  wp_link_pages( array(
                    'before' => '<div class="page-links">' . __( 'Pages:', 'larkin-square' ),
                    'after'  => '</div>',
                  ) );
                  ?>
                </div><!-- .entry-content -->

		<?php
		// get events for Food Truck Tuesdays
		get_template_part("inc/loop-tuesdays") ;
		?>		
              </article><!-- #post-## -->
            <?php
            // end of the loop.
            endwhile;
            ?>
            <section class="tuesday-sidebar">
              <ul class="group">
                <li class="group">
                  <div class="image-container">
                    <img src="<?php echo get_template_directory_uri()  ?>/img/ftt-sidebar/health-foundation.jpg"
                         alt="Independent Health Foundation"/>
                  </div>
                  <div class="text">
                    <p>
                      In a partnership with the Independent Health Foundation, every truck offers a Healthy Choice  menu option. Look for the Healthy Choice logo on a menu item at each truck.
                    </p>
                  </div>
                </li>

                <li class="group">
                  <div class="image-container">
                    <img src="<?php echo  get_template_directory_uri()  ?>/img/ftt-sidebar/winging.jpg"
                         alt=""/>
                  </div>
                  <div class="text">
                    <p>
                      Tune in Tuesday mornings to <a href="#">Winging It! Buffalo Style</a> on CW23 WNLO TV or catch a <a href="#">rebroadcast</a> online.
                    </p>
                  </div>
                </li>
		<li>
		  <?php larkin_get_featured_truck()  ?>
		</li>		
                <li class="group">
                  <div class="image-container">
		    <img src="<?php echo  get_template_directory_uri()  ?>/img/ftt-sidebar/drinks.jpg"
                         alt=""/>
		  </div>
                  <div class="text">
                    <p>
                     Beer and wine available for purchase weekly.
                    </p>
                  </div>
                </li>		
              </ul>
              <div class="youtube-container">
                <p>youtube embed goes here</p>
              </div>
            </section> <!-- ENDS .tuesdays-sidebar -->

          </div> <!-- ENDS .inner-row -->
        </div>
      </div><!-- ENDS .main-and-sidebar -->
      <?php
      // get truck slides
      larkin_trucks_slides() ;
      ?>
    </main><!-- #main -->
  </div><!-- #primary -->

  <?php get_template_part('inc/events-presented-by')  ?>
  
</div> <!-- ENDS .content-wrap -->
<?php get_footer(); ?>
