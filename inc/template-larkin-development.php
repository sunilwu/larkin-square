<?php
/**
 * The template for the "Larkin Development" page
*
 * Template Name: Larkin Development
*
 * @package larkin square
 */
get_header(); ?>
<div id="primary" class="content-area">
  <main id="main" class="site-main" role="main">

    <div class="inner-row">
    <article >
      <?php while ( have_posts() ) : the_post(); ?>
	<?php  get_template_part( 'content', 'page' ); ?>
    <?php endwhile; // end of the loop. ?>
    </article>    
    <?php
    /**
    /* Now get the listing
    */ ?>
    <div class="sidebar-third-image">
    <img src="<?php echo get_template_directory_uri()  ?>/img/larkin-development.jpg" alt="Larkin Development"/>
    </div>
</div>
  </main><!-- #main -->
</div><!-- #primary -->
<?php get_footer(); ?>
