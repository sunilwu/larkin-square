<?php
/**
 * The template for the "About Larkin Square" page
*
 * Template Name: About Larkin Square
*
 * @package larkin square
 */
get_header(); ?>
<div id="primary" class="content-area">
  <main id="main" class="site-main" role="main">

    <div class="inner-row">
    <article >
      <?php while ( have_posts() ) : the_post(); ?>
	<?php  get_template_part( 'content', 'page' ); ?>
    <?php endwhile; // end of the loop. ?>
    </article>    
    <?php
    /**
    /* Now get the listing
    */ ?>
    <div class="sidebar-third">
    Things To Do in Larkin Square
		<div id="thingstodo">
			<div><img src="/wp-content/themes/larkin-square/img/events/ToDo-Author-Series.png"></div>
			<div><img src="/wp-content/themes/larkin-square/img/events/ToDo-Larkin-Market.png"></div>
			<div><img src="/wp-content/themes/larkin-square/img/events/ToDo-Food-Truck-Tuesday.png"></div>
			<div><img src="/wp-content/themes/larkin-square/img/events/ToDo-LiveAtLarkin.png"></div>
		</div>
    </div>
</div>
  </main><!-- #main -->
</div><!-- #primary -->
<?php get_footer(); ?>
