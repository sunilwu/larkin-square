<?php
/**
 * The template for the "Directions and Map" page
 *
 * Template Name: Directions and Map
 *
 * @package larkin square
 */

get_header('pickleball'); ?>

<div id="primary" class="content-area">
  <main id="main" class="site-main" role="main">

    <div class="inner-row">
		<article >
		  <?php while ( have_posts() ) : the_post(); ?>
		<?php  get_template_part( 'content', 'page' ); ?>
		<?php endwhile; // end of the loop. ?>
		</article>
      <div class="sidebar-half">
		Larkin Square is centrally located near downtown Buffalo at 745 Seneca Street, or the intersection of Seneca, Swan and Van Rensselaer Streets. It is one mile southeast of the First Niagara Center, and just off the Smith Street and Hamburg Street exits of I-190.
		<br/><br/>
		<h2>Parking</h2>

		Parking is free in the designated Larkin Square parking lots during all public events. Simply look for the Free Parking Signs near and around Larkin Square. <br/>
		<br/>

		<h2>Driving Directions</h2>
		<br/>
		<h3>From Downtown Buffalo/Canalside</h3>
		Drive east on Exchange Street<br/>
		Turn left onto Van Rensselaer Street<br/>
		Turn right onto Seneca Street <br/>
		Larkin Square will be on your right<br/>
		<br/>
		<h3>From I-190</h3>
		Take Smith Street Exit #4<br/>
		Head north on Smith Street, towards Seneca Street<br/>
		Turn left onto Seneca Street<br/>
		Larkin Square will be on your left, just past Hydraulic Street<br/>
		<br/>
		<h3>From Route 33 W</h3>
		Take the Oak Street Exit<br/>
		Continue south on Oak Street <br/>
		Turn left onto Seneca Street, just before I-190 and Skyway entrances<br/>
		Larkin Square will be on your right <br/>
		<br/>
		<h3>Public Transportation</h3>
		Take Metro Rail towards downtown Buffalo to the Church Street Station<br/>
		Transfer to the NFTA Bus #15 (towards 15A Southgate) to Larkin Square<br/>
		Take the bus stop to Swan and Hagerman Streets<br/>
		Larkin Square will be on your right <br/>
		<br/>
		<h3>Riding Your Bike</h3>
		Larkin Square makes for a fun biking destination. Specially designed bike racks are located at the entrances to Larkin Square on Seneca Street and on Swan Street by Hydraulic Hearth.
		<br/><br/>

      </div>
      
    </div>   <!-- ENDS .inner-row -->

  </main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>
