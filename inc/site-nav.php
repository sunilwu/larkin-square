<?php
/*
 *
 * site navigation
*
*/
?>
<nav id="site-navigation" class="main-navigation" role="navigation">
  <button class="menu-toggle"><?php _e( 'Primary Menu', 'larkin-square' ); ?></button>
  <?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
</nav><!-- #site-navigation -->
