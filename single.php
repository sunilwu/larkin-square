<?php
/**
 * The template for the "Pickleball Rules" section
*  This can be used as a sidebar, or inserted into other templates
 *
 *
 * @package larkin square
 */  ?>
<?php get_header(); ?> 
<div class="content-wrap inner-row group">
<div id="primary" class="content-area">
  
  <main id="main" class="site-main" role="main">
    <?php while ( have_posts() ) : the_post(); ?>

      <?php  get_template_part( 'content', 'page' ); ?>

      <?php
      // If comments are open or we have at least one comment, load up the comment template
      if ( comments_open() || '0' != get_comments_number() ) :  
       comments_template();
      endif;
      ?>      
    <?php endwhile; // end of the loop. ?>
    
  </main><!-- #main -->
</div><!-- #primary -->

    <?php get_sidebar(); ?>
</div> <!-- ENDS .content-wrap -->
  <?php get_footer(); ?>
