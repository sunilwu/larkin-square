<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package larkin square
 */
?>
        </div><!-- #content -->

        <footer id="global-footer" class="footer" role="contentinfo">
          <div class="inner-row">
            <article id="footer-contact" class="footer-col">
              <header>
                <h3>Larkin Square</h3>
              </header>
              <ul class="contact-details">
                <li>754 Seneca Street</li>
                <li>Buffalo, NY 14210</li>
                <li>(716) 362-2665</li>
              </ul>
              <div class="footer-contact-links">
                <a href="#" class="first">Contact Us</a>
                <a href="#" class="second">Get Directions</a>
                <a href="/larkin-development" class="third"></a>                
              </div>
              
              
            </article> <!-- ENDS #footer-contact -->

	    <?php get_template_part('inc/loop-footer-news')  ?>

            <section id="outreach" class="group">
              <div class="row-one" class="group">
                <div id="social-ctas">
                  <h3>Social</h3>
                  <?php get_template_part('inc/social_links')  ?>
                </div>
                <div id="footer-email-invitation">
                  <section id="join-email-list" class="group email-footer">
                    <div class="first">
                      <input type="text" name="email-footer" id="email-footer" placeholder="Enter your email address">
                    </div>
                    <div class="second">
                      <span>Join Our Mailing List</span>
                    </div>
                  </section>
                </div>
              </div> <!-- ENDS .row-one -->

              <section id="footer-tweets">
                <h4>Fresh From Twitter</h4>
                <article>
                  <p>
                    Social Shopping coming to new <a href="#">@larkinsquare</a> market this summer. Looking for vendors! <a href="">pic.twitter/79DqkD1Rq2</a>
                  </p>
                </article>
                <article>
                  <p>
                    Social Shopping coming to new <a href="#">@larkinsquare</a> market this summer. Looking for vendors! <a href="">pic.twitter/79DqkD1Rq2</a>
                  </p>
                </article>
                <article>
                  <p>
                    Social Shopping coming to new <a href="#">@larkinsquare</a> market this summer. Looking for vendors! <a href="">pic.twitter/79DqkD1Rq2</a>
                  </p>
                </article>
              </section> <!-- ENDS #footer-tweets -->
            </section><!-- ENDS #outreach -->
          </div><!-- ENDS inner-row -->

          <div class="bottom-row inner-row">
            <article id="booking-cta" class="group">
              <div class="cta-text">
                <h3>Book Your Event At Larkin Square</h3>
                <section>
                  <p>
                    Larkin Square is perfect for weddings, corporate events, parties and happy hours. <a href="#">Book today!</a>
                  </p>
              </div>
              <div class="deck"></div>
                </section>
            </article>  <!-- ENDS #booking-cta -->
            <div class="footer-copyright">
              <p>©  2014 Larkin Square</p>
              <p class="renewal">A Renewal In Underway</p>
            </div>
          </div>  <!-- ENDS .bottom-row -->

        </footer><!-- #colophon -->
        <?php wp_footer(); ?>
</body>
</html>
