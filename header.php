<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package larkin square
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="generator" content="emacs">
<meta name="generator" content="Sunil Williams, sunil@sunil.co.nz">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php wp_head(); ?>
</head>
<?php
// pass in body classes
global $post ;
$slug = $post->post_name ;
?>
<body <?php body_class($slug); ?>>
  <?php
  // display the menu called Main Menu
  if ( dynamic_sidebar('mobile_nav_menu') ) : else : endif;  
  ?>

  <div id="masthead">  
        <div class="inner-row">
      <?php get_template_part('inc/ubermenu');  ?>
	</div>
  <header id="global-header" class="site-header">
    <div class="inner-row group">
      <div class="identity" class="group">
	<a href="<?php echo get_site_url()  ?>" class="logo-container">

	</a>
      </div>
      <div class="mobile-nav-button-container">
	<a href="#" class="mobile-nav-button"></a>
      </div>
      <div class="nav-and-social tplus">
	<?php get_template_part('inc/social_links')  ?>
	<?php get_template_part('inc/header_nav_template')  ?>
      </div>
    </div>
  </header><!-- ends #global-header  -->
  <?php   ?>
        <section id="mobile-social-links-header">
      	<?php get_template_part('inc/social_links')  ?>
	</section>
	<?php
	/***
	/* In this block of code, first we figure out which page we are on by inspecting the page slug
	/* Depending on which page we are on, we select the right image, or no image at all
	 */		
	if ( $slug == "live-at-larkin"  ) {   ?>
          <div class="masthead-page-logo">
	    <img src="<?php echo get_template_directory_uri()  ?>/img/logo-live-at-larkin.png"
		 alt="Live At Larkin"/>
	  </div>
	<?php      }   elseif ( $slug == "pickleball-courts"  )   {  ?>
          <div class="masthead-page-logo">
	    <img src="<?php echo get_template_directory_uri()  ?>/img/logo-pickleball.png"
		 alt="Live At Larkin"/>
	  </div>	
	<?php }
	      /****
	      /* End of image selection logic
	      /*
	       */	      
	?>
<?php
  	if ( $slug == "directions-and-parking" || $slug == "contact"  ) {   ?>
	<div id="masthead-map">
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyCCn6VfY5HhQBq-id7iYR8LKMktZb0ooss"></script>	
  	<script>
  	function initialize() {
		var larkinSquare = OP = new google.maps.LatLng(42.876109, -78.849595);
		 var mapOptions = {
			zoom: 15,
			center: larkinSquare,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			mapTypeControl				: false,
			panControl					: false,
			zoomControl					: false,
			streetViewControl			: false,
			minZoom: 14, maxZoom: 18, tilt: 0
		  }
		  var map = new google.maps.Map(document.getElementById("masthead-map"), mapOptions);
		var larkinMarkerImage = new google.maps.MarkerImage(
			'http://newlarkinsquare.dreamhosters.com/wp-content/themes/larkin-square/img/placemarker-larkinsquare.png', null, null, null, new google.maps.Size(72, 85)
		);  
		var larkinMarker = new google.maps.Marker({
		  position: larkinSquare,
		  map: map,
		  icon: larkinMarkerImage,
		  draggable:false
		});
		larkinMarker.setIcon(larkinMarkerImage);

		  
	}	
	google.maps.event.addDomListener(window, "load", initialize);

	</script>
	</div>
<?php	}?>
	
  </div> <!-- ENDS .masthead -->    
  <div id="content" class="site-content">
